Το παράδειγμα που βλέπουμε στο μάθημα 

CD Cart:
 - includes a simple html page with classical cds (Client side)
 
 - CD and Cart classes for the cds and the cart (Server side)

- Servlet to add CDs into a Cart (GET through a simple URLs in the html page)

- Same servlet deletes CDs from the Cart (POST through forms, attributes are hidden)

- Updates in cart are synchronized

- Cart is initialized in init, i.e. only once, when the servlet is initialized