package gr.csd.uoc.cs359.winter2017.cart.servlet;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import gr.csd.uoc.cs359.winter2017.cart.data.CD;
import gr.csd.uoc.cs359.winter2017.cart.data.Cart;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Panagiotis Papadakos <papadako at ics.forth.gr>
 */
@WebServlet(urlPatterns = {"/CartServlet"})
public class CartServlet extends HttpServlet {

    @Override
    public void init() {
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        HttpSession session = request.getSession(true);

        // Get cart
        Cart cart = (Cart) session.getAttribute("cart");

        if (cart == null) {
            cart = new Cart();
            session.setAttribute("cart", cart);
        }

        if (request.getParameter("action") != null && request.getParameter("action").equals("add")) {
            if (request.getParameter("description") != null
                    && request.getParameter("price") != null
                    && request.getParameter("id") != null) {

                CD cd = new CD(
                        request.getParameter("description"),
                        Float.parseFloat(request.getParameter("price")),
                        Integer.parseInt(request.getParameter("id")));

                response.setHeader("Refresh", "2;url=index.html");

                try (PrintWriter out = response.getWriter()) {
                    /* TODO output your page here. You may use following sample code. */
                    out.println("<!DOCTYPE html>");
                    out.println("<html>");
                    out.println("<head>");
                    out.println("<title>Servlet CartServlet</title>");
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<h1>Servlet CartServlet at " + request.getContextPath() + "</h1>");
                    out.println("You are trying to add the following cd:</br>");
                    out.println("CD ID:" + cd.getId()
                            + "</br>");
                    out.println("Description:" + cd.getDescription() + "</br>");
                    out.println("Price:" + cd.getPrice() + "</br>");

                    synchronized (this) {
                        boolean status = cart.add(cd);
                        out.println("Status: " + status + "</br>");
                    }

                    out.println(cart.print());

                    out.println("</body>");
                    out.println("</html>");
                }
            }
        } else if (request.getParameter("action") != null && request.getParameter("action").equals("remove")) {
            if (request.getParameter("id") != null) {
                response.setHeader("Refresh", "2;url=index.html");

                try (PrintWriter out = response.getWriter()) {
                    /* TODO output your page here. You may use following sample code. */
                    out.println("<!DOCTYPE html>");
                    out.println("<html>");
                    out.println("<head>");
                    out.println("<title>Servlet CartServlet</title>");
                    out.println("</head>");
                    out.println("<body>");
                    out.println("<h1>Servlet CartServlet at " + request.getContextPath() + "</h1>");
                    out.println("You are trying to remove the following cd:</br>");
                    out.println("CD ID:" + Integer.parseInt(request.getParameter("id"))
                            + "</br>");
                    synchronized (this) {
                        boolean status = cart.remove(Integer.parseInt(request.getParameter("id")));
                        out.println("Status: " + status + "</br>");
                    }

                    out.println(cart.print());

                    out.println("</body>");
                    out.println("</html>");
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
