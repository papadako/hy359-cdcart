/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.csd.uoc.cs359.winter2017.cart.data;

import java.util.HashMap;

/**
 * FORTH-ICS
 *
 * @author Panagiotis Papadakos <papadako at ics.forth.gr>
 */
public class Cart {

    private HashMap<Integer, CD> cart;

    public Cart() {
        cart = new HashMap<>();
    }

    public boolean add(CD cd) {
        if (cart.containsKey(cd.getId())) {
            return false;
        } else {
            cart.put(cd.getId(), cd);
        }

        return true;
    }

    public boolean remove(CD cd) {
        if (cart.containsKey(cd.getId())) {
            cart.remove(cd.getId());
            return true;
        } else {
            return false;
        }
    }

    public boolean remove(int id) {
        if (cart.containsKey(id)) {
            cart.remove(id);
            return true;
        } else {
            return false;
        }
    }
    
    
    public String print() {
        StringBuilder sb = new StringBuilder();

        sb.append("<H1>Your cart currently contains the following CDs:</h1>");
        sb.append("<table border=\"1\">");
        sb.append("<tr>");
        sb.append("<td><b>Id</b></td>");
        sb.append("<td><b>Description</b></td>");
        sb.append("<td><b>Price</b></td>");
        sb.append("</tr>");

        for (CD cd : cart.values()) {
            sb.append("<tr>");
            sb.append("<td>").append(cd.getId()).append("</td>");
            sb.append("<td>").append(cd.getDescription()).append("</td>");
            sb.append("<td>").append(cd.getPrice()).append("</td>");
            sb.append("</tr>");
        }
        
        sb.append("</table>");
        sb.append("</br>");
        
        return sb.toString();

    }

}
